class MoviesController < ApplicationController
  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def index    
    @movies = Movie.all
    @redirect = 0
    @sort_name = params[:sort].to_s
    
    #set the variable @all_ratings to be an enumerable collection of all possible values of a movie rating
    @all_ratings =  ['G','PG','PG-13','R']

    if(@checked != nil)
      @movies = @movies.find_all{ |m| @checked.has_key?(m.rating) and  @checked[m.rating]==true}      
    end
 
  #save/load sesstion sort state
  if( (@sort_name == 'title') or (@sort_name == 'release') )
    session[:sort] = params[:sort]
   elsif(session.has_key?(:sort) )
    params[:sort] = session[:sort]
    @redirect = 1
  end
 
   #now sort by sortName
   if(@sort_name == 'release')
      @movies = @movies.sort_by{|m| m.release_date.to_s }
   elsif (@sort_name == 'title')
       @movies = @movies.sort_by{|m| m.title }
   end
                   
    if(params[:ratings] != nil)
      session[:ratings] = params[:ratings] #save ratings to session
      @movies = @movies.find_all{ |m| params[:ratings].has_key?(m.rating) }
    elsif(session.has_key?(:ratings) )
      params[:ratings]= session[:ratings] #load ratings from session
      @redirect =1
    end
    
    @checked = {}
    @all_ratings.each { |rating|
      if (params[:ratings] == nil)
        @checked[rating] = false
      else
        @checked[rating] = params[:ratings].has_key?(rating)
      end
    }
    
    if(@redirect ==1)
      redirect_to movies_path(:sort=>params[:sort], :ratings =>params[:ratings] )
    end

  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
