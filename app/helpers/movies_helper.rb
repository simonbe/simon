module MoviesHelper
  # Checks if a number is odd:
  def oddness(count)
    count.odd? ?  "odd" :  "even"
  end
  
  def helper_class(field)
    if(params[:sort].to_s == field)
      return 'hilite'
    else
       return nil
    end
  end
  
  # Sort function
  def helper_sort(movie)
    if(params[:sort].to_s == 'release') #sort table by release date
      return movie.release_date.to_s
   elsif(params[:sort] == 'title') #sort table by title
      return movies.title
   end #end if
  end #end helper_sort
  
  
  def helper_check(rating)
     if(params[:ratings] == nil)
      return false
     end
    return params[:ratings].has_key?(rating)
  end
  
  def helper_select(movie)
    if(params[:ratings] == nil)
      return true
   else
     return params[:ratings].has_key?(movie.rating)
   end
  end
  
end
